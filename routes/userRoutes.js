const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');



// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// S38 Activity Solution
// Route for retrieving user details
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

    // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route for enrolling a user
/*router.post("/enroll", (req, res) => {
    let data = {
        userId: req.body.userId,
        courseId: req.body.courseId
    }

    userController.enroll(data).then(resultFromController => res.send(resultFromController))
});*/


router.post('/enroll', auth.verify, (req, res) => {

    let data = {
        userId: req.body.userId,
        courseId: req.body.courseId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    userController.enroll(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;