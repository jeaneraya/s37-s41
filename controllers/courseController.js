const Course = require('../models/Course');
const User = require('../models/User');

//Create a new course
/*module.exports.addCourse = (reqBody) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	// Saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation failed
		if(error) {

			return false

		// Course creation successful
		} else {

			return true
		}
	})
}*/

//Activity Solution 2
module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}


//Retrieve All Courses
module.exports.getAllCourses = (data) => {

	if (data.isAdmin) {
			return Course.find({}).then(result => {

			return result
		})
	} else {
		return false
	}
};



//Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
};



//Retrieve A Specific Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})

};


//Update A Specific Course
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	}

	// Syntax
			// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) =>{

		// Course not updated
		if(error) {

			return false

		// Course updated successfully
		} else {

			return true
		
		}

	})

};


// Session 40 - Activity - Solution
// Archive A Course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently

module.exports.archiveCourse = (data) => {

	return Course.findById(data.courseId).then((result, err) => {

		if(data.isAdmin === true) {

			result.isActive = false;

			return result.save().then((archivedCourse, err) => {

				// Course not archived
				if(err) {

					return false;

				// Course archived successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}

// S40 Solution 2:
// module.exports.archiveCourse = (data, reqBody) => {

// 	if (data.payload === true) {

// 		let updateActiveField = {
// 			isActive: reqBody.isActive
// 		}

// 		return Course.findByIdAndUpdate(data.courseId, updateActiveField).then((course, err) => {

// 				if(err) {
				
// 					return false
// 				}  else {

// 					return true
// 				}
// 		})
		 
// 	} else {

// 		return false
// 	} 

// }
